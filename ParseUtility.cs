using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
namespace Reader
{
    public class ParseUtility
    {
        private const string exceptionType = "Exception Type:";
        private const string exception = "Exception:";
        private const string source = "Source:";
        private const string stackTrace = "Stack Trace:";
        private const string timeRegularExpresion = @"\[(1[0-2]|0?[1-9]):[0-5][0-9]:[0-5][0-9] (AM|PM)\]";
        private const string textModifier = ".txt";
        private const string inner = "Inner";
        public static void updateValue(string type, string value, ApiLogHistory apiLogHistory)
        {
            switch (type)
            {
                case exceptionType:
                    apiLogHistory.exceptionType = apiLogHistory.exceptionType + Environment.NewLine + value;
                    break;
                case exception:
                    apiLogHistory.exception = apiLogHistory.exception + Environment.NewLine + value;
                    break;
                case source:
                    apiLogHistory.source = apiLogHistory.source + Environment.NewLine + value;
                    break;
                case stackTrace:
                    apiLogHistory.stackTrace = apiLogHistory.stackTrace + Environment.NewLine + value;
                    break;
                default:
                    Console.WriteLine("Default case " + type);
                    break;
            }
        }

        public static bool checkContainsType(String lines)
        {
            return (lines.Contains(exception)
                             || lines.Contains(source)
                             || lines.Contains(exceptionType)
                             || lines.Contains(stackTrace));
        }

        public static List<ApiLogHistory> parseFile(String logFolderPath)
        {
            Console.WriteLine("parsing");
            Regex regex = new Regex(timeRegularExpresion);
            //List<ApiLogHistory> apiLogHistories=new ArrayList<>();
            List<ApiLogHistory> apiLogHistories = new List<ApiLogHistory>();
            //open folder
            //for each folder in folder
            string[] subdirs1 = Directory.GetDirectories(logFolderPath);
            foreach (string subdir1 in subdirs1)
            {
                //for each folder in folder
                string[] subdirs2 = Directory.GetDirectories(subdir1);
                foreach (string subdir2 in subdirs2)
                {
                    //date=folder name, apiLogHistory.name=folder name;
                    DateTime dateTime = DateTime.Parse(Path.GetFileName(subdir2));
                    //open folder
                    string[] filePaths = Directory.GetFiles(subdir2);
                    //for each file in folder
                    foreach (string filePath in filePaths)
                    {
                        // //logtype=filename,...
                        string logtype = Path.GetFileName(filePath).Replace(textModifier, "");
                        //initialize api log history object
                        ApiLogHistory apiLogHistory = null;
                        //DateTime dateTime1 = new DateTime();
                        string[] lines = File.ReadAllLines(@filePath, Encoding.UTF8);
                        String type = exceptionType;
                        for (int i = 0; i < lines.Length; i++)
                        {
                            lines[i]= lines[i].Replace("\u0000", "").Replace("\0", "").Replace("0x00","");
                            if((i+1)<lines.Length)
                            lines[i+1]=lines[i+1].Replace("\u0000", "").Replace("\0", "").Replace("0x00","");
                            if (lines[i].Contains(exceptionType))
                            {
                                type = exceptionType;
                                // Console.WriteLine("-------------------------");
                                if (apiLogHistory != null)
                                {
                                    apiLogHistories.Add(apiLogHistory);
                                }
                                apiLogHistory = new ApiLogHistory();
                                apiLogHistory.createdDate = dateTime;
                                apiLogHistory.logType = logtype;
                                if ((i + 1) != lines.Length && !checkContainsType(lines[i + 1]))
                                {
                                    apiLogHistory.exceptionType = lines[i + 1];
                                    // Console.WriteLine("Exception type: "+lines[i+1]);
                                    i++;
                                }
                                else
                                {
                                    Console.WriteLine(exceptionType + " " + lines[i].Replace(exceptionType, "").Replace(inner, ""));
                                    // apiLogHistory.exceptionType=lines[i].Replace("Exception type:","").Replace("Inner","");
                                }
                            }
                            else if (lines[i].Contains(exception))
                            {
                                type = exception;
                                if ((i + 1) != lines.Length && !checkContainsType(lines[i + 1]))
                                {
                                    // Console.WriteLine("Exception: "+lines[i+1]);
                                    apiLogHistory.exception = lines[i + 1];
                                    i++;
                                }
                                else
                                {
                                    //  Console.WriteLine("Exception: "+lines[i].Replace("Exception: ","").Replace("Inner",""));
                                    apiLogHistory.exception = lines[i].Replace(exception + " ", "").Replace(inner, "");
                                }

                            }
                            else if (lines[i].Contains(source))
                            {
                                type = source;
                                if ((i + 1) != lines.Length && !checkContainsType(lines[i + 1]))
                                {
                                    // Console.WriteLine("Source: "+lines[i+1]);
                                    apiLogHistory.source = lines[i + 1];
                                    i++;
                                }
                                else
                                {
                                    // Console.WriteLine("Source: "+lines[i].Replace("Source: ","").Replace("Inner",""));
                                    apiLogHistory.source = lines[i].Replace(source + " ", "").Replace(inner, "");
                                }
                            }
                            else if (lines[i].Contains(stackTrace))
                            {
                                type = stackTrace;
                                if ((i + 1) != lines.Length && !checkContainsType(lines[i + 1]))
                                {
                                    //  Console.WriteLine("Stack Trace: "+lines[i+1]);
                                    apiLogHistory.stackTrace = lines[i + 1];
                                    i++;
                                }
                                else
                                {
                                    //  Console.WriteLine("Stack Trace: "+lines[i].Replace("Stack Trace: ","").Replace("Inner",""));
                                    apiLogHistory.stackTrace = lines[i].Replace(stackTrace + " ", "").Replace(inner, "");
                                }
                            }
                            else if (regex.IsMatch(lines[i]))
                            {
                                Match match = regex.Match(lines[i]);
                                //     Console.WriteLine("Time is: "+ DateTime.ParseExact(match.Value.Replace("[","").Replace("]",""),"h:mm:ss tt",null));
                                DateTime dateTime2 = DateTime.ParseExact(match.Value.Replace("[", "").Replace("]", ""), "h:mm:ss tt", null);
                                dateTime = dateTime.Date + dateTime2.TimeOfDay;
                                // Console.WriteLine(dateTime);
                            }
                            else if (lines[i].Equals("")||lines[i]==null)
                            {
                                if(lines[i]==null)
                                {
                                    Console.WriteLine("null2");
                                }
                                Console.WriteLine("null");
                            }
                            else
                            {
                                // Console.WriteLine(type+" "+lines[i]);
                                if (apiLogHistory == null)
                                {
                                      Console.WriteLine("skip");
                                }
                                else
                                {
                                    updateValue(type, lines[i], apiLogHistory);
                                }
                            }
                            if (i == (lines.Length - 1) && apiLogHistory != null)
                            {
                                apiLogHistories.Add(apiLogHistory);
                            }
                        }
                    }
                }

            }
            Console.WriteLine("end parsing");
            return apiLogHistories;
        }


    }
}