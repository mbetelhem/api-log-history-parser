using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
namespace Reader
{
    public class LogContext : DbContext
    {
        public DbSet<ApiLogHistory> apiLogHistories { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("User ID=pharm;Password=2L5/agHn;Host=192.168.2.80;Port=5432;Database=iimport_log;Pooling=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApiLogHistory>(entity =>
            {
                entity.Property(cd => cd.createdDate);
                entity.Property(cd => cd.logType);
                entity.Property(cd => cd.exceptionType);
                entity.Property(cd => cd.exception);
                entity.Property(cd => cd.source);
                entity.Property(cd => cd.stackTrace);
            });
        }
    }
}