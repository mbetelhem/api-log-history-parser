﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Reader
{
    class Program
    {
        static void Main(string[] args)
        {
            //string logFolderPath = "C:\\Users\\Berhanu Mengesha\\Downloads\\Telegram Desktop\\Logs\\Logs";
            string logFolderPath = "F:\\Logs\\Logs";
            //string logFolderPath2 = "C:\\Users\\Berhanu Mengesha\\Downloads\\";
            List<ApiLogHistory> apiLogHistories = ParseUtility.parseFile(logFolderPath);
            LogContext logContext=new LogContext();
            logContext.apiLogHistories.AddRange(apiLogHistories);
            logContext.SaveChanges();
        }
    }
}
