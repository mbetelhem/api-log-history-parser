using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Reader
{
    [Table("api_log_history", Schema = "public")]
    public class ApiLogHistory
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("created_date")]
        public DateTime createdDate{get; set;}
        [Column("log_type")]
        public string logType{get;set;}
        [Column("exception_type")]
        public string exceptionType{get;set;}
        [Column("exception")]
        public string exception{get;set;}
        [Column("source")]
        public string source{get;set;}
        [Column("stack_trace")]
        public string stackTrace{get;set;}
    }
}